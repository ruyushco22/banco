<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mapas extends CI_Controller {

    public function __construct() {
        parent::__construct();
        // Carga el modelo necesario para los mapas aquí
        $this->load->model('Mapa');
        $this->load->model("Agencia");
    }
    public function index()
    {
        $data["listadoAgencias"] = $this->Mapa->consultarTodos();
        $data["listadoCajeros"] = $this->Mapa->consultarTodo();
        $data["listadoCorresponsales"] = $this->Mapa->consultarTod();
        $this->load->view("header");
        $this->load->view("mapas/index", $data);
        $this->load->view("footer");
    }
}
?>
