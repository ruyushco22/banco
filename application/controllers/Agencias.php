<?php

class Agencias extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model("Agencia");
    }
    // Renderización de la vista index de agencias
    public function index()
    {
        $data["listadoAgencias"] = $this->Agencia->consultarTodos();
        $this->load->view("header");
        $this->load->view("agencias/index", $data);
        $this->load->view("footer");
    }


    // Eliminar agencia
    public function borrar($id)
    {
        $this->Agencia->eliminar($id);
        $this->session->set_flashdata("confirmacion", "Agencia eliminada exitosamente");
        redirect("agencias/index");
    }

    // Renderización de formulario de nueva agencia
    public function nuevo()
    {
        $this->load->view("header");
        $this->load->view("agencias/nuevo");
        $this->load->view("footer");
    }

    // Capturar datos e insertar agencia
    public function guardarAgencia()
    {
        $datosNuevaAgencia = array(
            "nombre" => $this->input->post("nombre"),
            "direccion" => $this->input->post("direccion"),
            "ciudad" => $this->input->post("ciudad"),
            "estado" => $this->input->post("estado"),
            "telefono" => $this->input->post("telefono"),
            "fechaApertura" => $this->input->post("fechaApertura"),
            "gerente" => $this->input->post("gerente"),
            "latitud" => $this->input->post("latitud"),   // Valor predeterminado si no está presente
            "longitud" => $this->input->post("longitud")
        );
        $this->Agencia->insertar($datosNuevaAgencia);
        $this->session->set_flashdata("confirmacion", "Agencia guardada exitosamente");
        enviarEmail("eddyushco@gmail.com","CREACION",
          "<h1>SE CREO LA AGENCIA </h1>".$datosNuevaAgencia['nombre']);
        redirect('agencias/index');
    }

    // Editar agencia
    public function editar($id)
    {
        $data["agenciaEditar"] = $this->Agencia->obtenerPorId($id);
        $this->load->view("header");
        $this->load->view("agencias/editar", $data);
        $this->load->view("footer");
    }

    // Actualizar datos de la agencia
    public function actualizarAgencia()
    {
        $id = $this->input->post("id");
        $datosAgencia = array(
            "nombre" => $this->input->post("nombre"),
            "direccion" => $this->input->post("direccion"),
            "ciudad" => $this->input->post("ciudad"),
            "estado" => $this->input->post("estado"),
            "telefono" => $this->input->post("telefono"),
            "fechaApertura" => $this->input->post("fechaApertura"),
            "gerente" => $this->input->post("gerente"),
            "latitud" => $this->input->post("latitud"),   // Valor predeterminado si no está presente
            "longitud" => $this->input->post("longitud")
        );
        $this->Agencia->actualizar($id, $datosAgencia);
        $this->session->set_flashdata("confirmacion", "Agencia actualizada exitosamente");
        redirect('agencias/index');
    }

}//cierre de la clase
