<?php

  class Corresponsales extends CI_Controller
  {

    function __construct()
    {
      parent::__construct();
      $this->load->model("Corresponsal");
    }
    //renderizacion de la vista index de hospitales
    public function index(){
      $data["listadoCorresponsales"]=$this->Corresponsal->consultarTodos();
      $this->load->view("header");
      $this->load->view("corresponsales/index",$data);
      $this->load->view("footer");
    }
    //eliminar
    public function borrar($id_ae){
      $this->Corresponsal->eliminar($id_ae);
      $this->session->set_flashdata("confirmacion","Corresponsal elimina exitosamente");
      redirect("corresponsales/index");
    }
    // Renderizacion de formulario de nuevo hospitales
    public function nuevo(){
      $this->load->view("header");
      $this->load->view("corresponsales/nuevo");
      $this->load->view("footer");
    }
    //capturando datos insertando hospital
    public function guardarCorresponsal(){

      $datosNuevoCorresponsal = array(
        "nombres"=>$this->input->post("nombres"),
        "apellidos"=>$this->input->post("apellidos"),
        "direccion"=>$this->input->post("direccion"),
        "ciudad"=>$this->input->post("ciudad"),
        "telefono"=>$this->input->post("telefono"),
        "correo"=>$this->input->post("correo"),
        "fechaAcuerdo"=>$this->input->post("fechaAcuerdo"),
        "latitud" => $this->input->post("latitud"),   // Valor predeterminado si no está presente
        "longitud" => $this->input->post("longitud"),
      );
      $this->Corresponsal->insertar($datosNuevoCorresponsal);
      $this->session->set_flashdata("confirmacion","Corresponsal guardad exitosamente");
      redirect('corresponsales/index');
    }

    public function editar($id){
      $data["corresponsalEditar"]=$this->Corresponsal->obtenerPorId($id);
      $this->load->view("header");
      $this->load->view("corresponsales/editar",$data);
      $this->load->view("footer");
    }
    public function actualizarCorresponsal(){
      $id=$this->input->post("id");
      $datosCorresponsal=array(
        "nombres"=>$this->input->post("nombres"),
        "apellidos"=>$this->input->post("apellidos"),
        "direccion"=>$this->input->post("direccion"),
        "ciudad"=>$this->input->post("ciudad"),
        "telefono"=>$this->input->post("telefono"),
        "correo"=>$this->input->post("correo"),
        "fechaAcuerdo"=>$this->input->post("fechaAcuerdo"),
        "latitud" => $this->input->post("latitud"),   // Valor predeterminado si no está presente
        "longitud" => $this->input->post("longitud"),
      );
      $this->Corresponsal->actualizar($id,$datosCorresponsal);
      $this->session->set_flashdata("confirmacion",
      "Corresponsal actualizado exitosamente");
      redirect('corresponsales/index');
    }
  }//cierre de la clase





 ?>
