<?php

  class Cajeros extends CI_Controller
  {

    function __construct()
    {
      parent::__construct();
      $this->load->model("Cajero");
      $this->load->model("Agencia");
    }
    //renderizacion de la vista index de cajeros
    public function index(){
      $data["listadoCajeros"] = $this->Cajero->consultarTodosConAgencia();
      $this->load->view("header");
      $this->load->view("cajeros/index",$data);
      $this->load->view("footer");
    }
    //eliminar
    public function borrar($idCajero){
      $this->Cajero->eliminar($idCajero);
      $this->session->set_flashdata("confirmacion", "Cajero eliminado exitosamente");
      redirect("cajeros/index");
    }
    // Renderizacion de formulario de nuevo cajeros
    public function nuevo(){
        $this->load->model("Agencia"); // Asegúrate de cargar el modelo de Agencia si no lo has hecho antes
        $data["agencias"] = $this->Agencia->consultarTodos(); // Obtener la información de las agencias
        $this->load->view("header");
        $this->load->view("cajeros/nuevo", $data); // Pasar los datos de las agencias a la vista
        $this->load->view("footer");
    }
    //capturando datos insertando hospital
    public function guardarCajero(){


      $datosNuevoCajero = array(
        "modelo"=>$this->input->post("modelo"),
        "numeroSerie"=>$this->input->post("numeroSerie"),
        "ciudad"=>$this->input->post("ciudad"),
        "fechaInstalacion"=>$this->input->post("fechaInstalacion"),
        "id"=>$this->input->post("id"),
        "latitud"=>$this->input->post("latitud"),
        "longitud"=>$this->input->post("longitud"),
        "estado"=>$this->input->post("estado"),


      );
      $this->Cajero->insertar($datosNuevoCajero);
      //flash_data -> Crear una session de tipo flash
      $this->session->set_flashdata("confirmacion","Cajero gurdado exitosamente");

      redirect("cajeros/index");
    }

    public function editar($id){
      $data["cajeroEditar"]=$this->Cajero->obtenerPorId($id);
      $data["agencias"] = $this->Agencia->consultarTodos();
      $this->load->view("header");
      $this->load->view("cajeros/editar", $data); // Cambia la vista a editar si tienes una vista específica para ello
      $this->load->view("footer");
    }

    public function actualizarCajero(){
      $idCajero=$this->input->post("idCajero");

      $datosCajero=array(
        "modelo"=>$this->input->post("modelo"),
        "numeroSerie"=>$this->input->post("numeroSerie"),
        "ciudad"=>$this->input->post("ciudad"),
        "fechaInstalacion"=>$this->input->post("fechaInstalacion"),
        "id"=>$this->input->post("id"),
        "latitud"=>$this->input->post("latitud"),
        "longitud"=>$this->input->post("longitud"),
        "estado"=>$this->input->post("estado"),
      );

      $this->Cajero->actualizar($idCajero,$datosCajero);
      $this->session->set_flashdata("confirmacion",
      "Cajero actualizado exitosamente");
      redirect('cajeros/index');
    }
  }//cierre de la clase

 ?>
