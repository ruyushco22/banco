<br>
<div class="text-center">
    <h1>
      <i class="fas fa-user"></i>
      <b>CORRESPONSALES</b>
    </h1>
</div>
<div class="row">
  <div class="col-md-12 text-end">

    <a href="<?php  echo site_url('corresponsales/nuevo'); ?>" class="btn btn-outline-success"><i class="fa fa-plus-circle"></i> Agregar Corresponsal</a>
    <br> <br>
  </div>

</div>
<?php if ($listadoCorresponsales): ?>
    <table class="table table-bordered">
        <thead>
              <tr class="text-center">
                <th>ID</th>
                <th>NOMBRES</th>
                <th>APELLIDOS</th>
                <th>DIRECCIÓN</th>
                <th>CIUDAD</th>
                <th>TELEFONO</th>
                <th>CORREO</th>
                <th>FECHA ACUERDO</th>
                <th>LATITUD</th>
                <th>LONGITUD</th>
                <th>ACCIONES</th>
              </tr>
        </thead>
        <tbody>
            <?php foreach ($listadoCorresponsales as $corresponsal): ?>
                <tr class="text-center">
                  <td><?php echo $corresponsal->id; ?></td>
                  <td><?php echo $corresponsal->nombres; ?></td>
                  <td><?php echo $corresponsal->apellidos; ?></td>
                  <td><?php echo $corresponsal->direccion; ?></td>
                  <td><?php echo $corresponsal->ciudad; ?></td>
                  <td><?php echo $corresponsal->telefono; ?></td>
                  <td><?php echo $corresponsal->correo; ?></td>
                  <td><?php echo $corresponsal->fechaAcuerdo; ?></td>
                  <td><?php echo $corresponsal->latitud; ?></td>
                  <td><?php echo $corresponsal->longitud; ?></td>
                  <td>
                    <a href="<?php echo site_url('corresponsales/editar/').$corresponsal->id; ?>"
                         class="btn btn-warning"
                         title="Editar">
                      <i class="fa fa-pen"></i>
                    </a>
                    &nbsp&nbsp
                    <a href="<?php echo site_url('corresponsales/borrar/') . $corresponsal->id; ?>" class="btn btn-danger delete-btn" title="Borrar">
                        <i class="fa-solid fa-trash"></i>
                    </a>
                  </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <script type="text/javascript">
    // Script para mostrar el cuadro de diálogo de confirmación antes de eliminar un hospital
    $('.delete-btn').on('click', function(e) {
        e.preventDefault();
        var url = $(this).attr('href');
        Swal.fire({
            title: "CONFIRMACIÓN",
            text: "¿Estás seguro de que deseas eliminar este corresponsal?",
            icon: "question",
            showCancelButton: true,
            confirmButtonColor: "#3085d6",
            cancelButtonColor: "#d33",
            confirmButtonText: "Sí",
            cancelButtonText: "No"
        }).then((result) => {
            if (result.isConfirmed) {
                window.location.href = url; // Redirige al URL de eliminación si se confirma
            }
        });
    });
</script>

    

<?php else: ?>
  <div class="alert alert-danger">
      No se encontro hospitales registrados
  </div>
<?php endif; ?>
