
<div class="container">
<h1><i class="fa-solid fa-pen-to-square"></i> <b>EDITAR CORRESPONSAL</b></h1><br>
<form class="row g-3 needs-validation custom-width-form" id="formulario_corresponsal" action="<?php echo site_url('corresponsales/actualizarCorresponsal'); ?>" method="post" enctype="multipart/form-data">
		<input type="hidden" name="id" id="id" value="<?php echo $corresponsalEditar->id; ?>">
		<div class="col-md-4">
        <label for="nombre" class="form-label white-text"> <b>NOMBRES:</b> </label>
        <input type="text" name="nombres" id="nombres" value="<?php echo $corresponsalEditar->nombres; ?>" class="form-control" placeholder="Ingrese los nombres" required>
    </div>
    <div class="col-md-4">
        <label for="nombre" class="form-label white-text"> <b>APELLIDOS:</b> </label>
        <input type="text" name="apellidos" id="apellidos" value="<?php echo $corresponsalEditar->apellidos; ?>" class="form-control" placeholder="Ingrese los apellidos" required>
    </div>
    <div class="col-md-4">
        <label for="direccion" class="form-label white-text"> <b>DIRECCIÓN:</b> </label>
        <input type="text" name="direccion" id="direccion" value="<?php echo $corresponsalEditar->direccion; ?>" class="form-control" placeholder="Ingrese la dirección" required>
    </div>
    <div class="col-md-4">
        <label for="ciudad" class="form-label white-text"> <b>CIUDAD:</b> </label>
        <input type="text" name="ciudad" id="ciudad" value="<?php echo $corresponsalEditar->ciudad; ?>" class="form-control" placeholder="Ingrese la ciudad" required>
    </div>
    <div class="col-md-4">
        <label for="telefono" class="form-label white-text"> <b>TELÉFONO:</b> </label>
        <input type="number" name="telefono" id="telefono" value="<?php echo $corresponsalEditar->telefono; ?>" class="form-control" placeholder="Ingrese el número" required>
    </div>
    <div class="col-md-4">
        <label for="gerente" class="form-label white-text"> <b>CORREO:</b> </label>
        <input type="email" name="correo" id="correo" value="<?php echo $corresponsalEditar->correo; ?>" class="form-control" placeholder="Ingrese el correo" required>
    </div>
    <div class="col-md-4">
        <label for="fechaApertura" class="form-label white-text"> <b>FECHA ACUERDO:</b> </label>
        <input type="date" name="fechaAcuerdo" id="fechaAcuerdo" value="<?php echo $corresponsalEditar->fechaAcuerdo; ?>" class="form-control" placeholder="Seleccione la fecha del acuerdo" required>
    </div>
    <div class="row">
        <div class="col-md-6">
            <label for=""> <b>Latitud:</b> </label>
            <input type="number" step="any" name="latitud" id="latitud" value="<?php echo $corresponsalEditar->latitud; ?>" class="form-control" placeholder="Ingrese la latitud" readonly>
        </div>
        <div class="col-md-6">
            <label for=""> <b>Longitud:</b> </label>
            <input type="number" step="any" name="longitud" id="longitud" value="<?php echo $corresponsalEditar->longitud; ?>" class="form-control" placeholder="Ingrese el longitud" readonly>
        </div>
    </div>
    <br><br><br>
    <div class="row">
        <div class="col-md-12">
            <div id="mapa" style="height:350px; width:100%; border:1px solid black;">
            </div> <br>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 text-center">
            <button type="submit" name="button" class="btn btn-warning"><i class="fa fa-pen fa-bounce"></i> &nbsp Editar</button> &nbsp;&nbsp;&nbsp;&nbsp;
            <a href="<?php echo site_url('corresponsales/index'); ?>" class="btn btn-danger"><i class="fa fa-xmark-circle fa-spin"></i> Cancelar</a>
        </div>
    </div>
</form>


<br>
<br>
<script type="text/javaScript">
  function initMap(){
    var coordenadaCentral =
		new google.maps.LatLng(<?php echo $corresponsalEditar->latitud; ?>, <?php echo $corresponsalEditar->longitud; ?>);
   var miMapa= new google.maps.Map(
     document.getElementById('mapa'),{
       center: coordenadaCentral,
       zoom: 10,
       mapTypeId: google.maps.MapTypeId.ROADMAP
     }
   );
   var marcador= new google.maps.Marker({
     position:coordenadaCentral,
     map: miMapa,
     title: 'Seleccione la ubicacion',
     draggable:true
   });
   google.maps.event.addListener(
    marcador,
    'dragend',
    function(event){
      var latitud=this.getPosition().lat();
      var longitud=this.getPosition().lng();
      document.getElementById('latitud').value=latitud;
      document.getElementById('longitud').value=longitud;
    }
   );
  }

</script>
<script>
	$(document).ready(function() {
			// Inicialización del plugin Bootstrap Fileinput
			$("#carnet").fileinput({
					language: 'es',
					maxFileSize: 0
			});
	});
</script>
<script type="text/javascript">
$(document).ready(function() {
    $('#formulario_corresponsal').validate({
      rules: {
          nombres: {
              required: true,
              letras: true,
              minlength: 3,
              formatoNombre: true
          },
          apellidos: {
              required: true,
              letras: true,
              minlength: 3,
              formatoNombre: true
          },
          direccion: {
              required: true
          },
          ciudad: {
              required: true,
              letras:true,
              minlength: 3,
              primeraLetraMayuscula: true
          },
          telefono: {
              required: true,
              minlength: 10,
              maxlength: 10,
              digits: true
          },
          correo: {
              required: true,
              email: true
          },
          fechaAcuerdo: {
              required: true,
              date: true,
              min: "1960-01-01",
              max: "2024-12-31"
          },
          latitud: {
              required: true,
              number: true
          },
          longitud: {
              required: true,
              number: true
          }
      },
      messages: {
          nombres: {
              required: "Por favor ingrese los nombres",
              letras: "Por favor ingrese solo letras",
              minlength: "El nombre debe tener al menos {0} caracteres",
              formatoNombre: "Ingrese un nombre válido (cada palabra debe comenzar con mayúscula)",
          },
          apellidos: {
              required: "Por favor ingrese los apellidos",
              letras: "Por favor ingrese solo letras",
              minlength: "El nombre debe tener al menos {0} caracteres",
              formatoNombre: "Ingrese un nombre válido (cada palabra debe comenzar con mayúscula)",
          },
          direccion: {
              required: "Por favor ingrese la dirección"
          },
          ciudad: {
              required: "Por favor ingrese la ciudad",
              minlength: "El nombre debe tener al menos {0} caracteres"
          },
          telefono: {
              required: "Por favor ingrese el teléfono",
              maxlength: "Por favor ingrese el número de 10 digitos",
              minlength: "Por favor ingrese el número de 10 digitos"
          },
          correo: {
              required: "Por favor ingrese el correo",
              email: "Ingrese un correo válido"
          },
          fechaAcuerdo: {
              required: "Por favor ingrese la fecha del acuerdo",
              max: "Fecha fuera de rango (1960-2024)",
              min: "Fecha fuera de rango (1960-2024)"
          },
          latitud: {
              required: "Por favor ingrese la latitud",
              number: "Ingrese un número válido"
          },
          longitud: {
              required: "Por favor ingrese la longitud",
              number: "Ingrese un número válido"
          }
      },
      errorElement: 'div',
      errorPlacement: function(error, element) {
          error.addClass('invalid-feedback');
          element.addClass('is-invalid');
          error.insertAfter(element);
      },
      highlight: function(element, errorClass, validClass) {
          $(element).addClass('is-invalid');
      },
      unhighlight: function(element, errorClass, validClass) {
          $(element).removeClass('is-invalid');
      }
  });
  $.validator.addMethod("letras", function(value, element) {
    return this.optional(element) || /^[a-zA-Z\sáéíóúÁÉÍÓÚüÜñÑ]+$/.test(value);
  }, "Solo se permiten letras");
  $.validator.addMethod("primeraLetraMayuscula", function(value, element) {
        return this.optional(element) || /^[A-Z].*/.test(value);
  }, "La primera letra del nombre debe ser mayúscula");
  $.validator.addMethod("formatoNombre", function(value, element) {
        return this.optional(element) || /^[A-Z][a-z]+\s[A-Z][a-z]+$/.test(value);
    }, "Cada palabra debe comenzar con mayúscula");
});

</script>
