<br>
<div class="text-center">
    <h1>
      <i class="fas fa-building"></i>
      <b>AGENCIAS</b>
    </h1>
</div>
<div class="row">
  <div class="col-md-12 text-end">

    <a href="<?php  echo site_url('agencias/nuevo'); ?>" class="btn btn-outline-success"><i class="fa fa-plus-circle"></i> Agregar Agencia</a>
    <br> <br>
  </div>

</div>
<?php if ($listadoAgencias): ?>
    <table class="table table-bordered">
        <thead>
              <tr class="text-center">
                <th>ID</th>
                <th>NOMBRE</th>
                <th>DIRECCIÓN</th>
                <th>CIUDAD</th>
                <th>ESTADO</th>
                <th>TELEFONO</th>
                <th>FECHA APERTURA</th>
                <th>GERENTE RESPONSABLE</th>
                <th>LATITUD</th>
                <th>LONGITUD</th>
                <th>ACCIONES</th>
              </tr>
        </thead>
        <tbody>
            <?php foreach ($listadoAgencias as $agencia): ?>
                <tr class="text-center">
                  <td><?php echo $agencia->id; ?></td>
                  <td><?php echo $agencia->nombre; ?></td>
                  <td><?php echo $agencia->direccion; ?></td>
                  <td><?php echo $agencia->ciudad; ?></td>
                  <td><?php echo $agencia->estado; ?></td>
                  <td><?php echo $agencia->telefono; ?></td>
                  <td><?php echo $agencia->fechaApertura; ?></td>
                  <td><?php echo $agencia->gerente; ?></td>
                  <td><?php echo $agencia->latitud; ?></td>
                  <td><?php echo $agencia->longitud; ?></td>
                  <td>
                    <a href="<?php echo site_url('agencias/editar/').$agencia->id; ?>"
                         class="btn btn-warning"
                         title="Editar">
                      <i class="fa fa-pen"></i>
                    </a>
                    &nbsp&nbsp
                    <a href="<?php echo site_url('agencias/borrar/') . $agencia->id; ?>" class="btn btn-danger delete-btn" title="Borrar">
                        <i class="fa-solid fa-trash"></i>
                    </a>
                  </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <script type="text/javascript">
    // Script para mostrar el cuadro de diálogo de confirmación antes de eliminar un hospital
    $('.delete-btn').on('click', function(e) {
        e.preventDefault();
        var url = $(this).attr('href');
        Swal.fire({
            title: "CONFIRMACIÓN",
            text: "¿Estás seguro de que deseas eliminar esta agencia?",
            icon: "question",
            showCancelButton: true,
            confirmButtonColor: "#3085d6",
            cancelButtonColor: "#d33",
            confirmButtonText: "Sí",
            cancelButtonText: "No"
        }).then((result) => {
            if (result.isConfirmed) {
                window.location.href = url; // Redirige al URL de eliminación si se confirma
            }
        });
    });
</script>

  
<?php else: ?>
  <div class="alert alert-danger">
      No se encontro hospitales registrados
  </div>
<?php endif; ?>
