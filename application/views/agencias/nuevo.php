<div class="container">
<h1>
  <br>
  <b>
    <i class="fa fa-plus-circle"></i>
    NUEVA AGENCIA
  </b>
</h1>
<br>
<form class="row g-3 needs-validation custom-width-form" id="formulario_agencia" action="<?php echo site_url('agencias/guardarAgencia'); ?>" method="post" enctype="multipart/form-data" >
    <div class="col-md-4">
        <label for="nombre" class="form-label white-text"> <b>NOMBRE:</b> </label>
        <input type="text" name="nombre" id="nombre" value="" class="form-control" placeholder="Ingrese el nombre" required>
    </div>
    <div class="col-md-4">
        <label for="direccion" class="form-label white-text"> <b>DIRECCIÓN:</b> </label>
        <input type="text" name="direccion" id="direccion" value="" class="form-control" placeholder="Ingrese la dirección" required>
    </div>
    <div class="col-md-4">
        <label for="ciudad" class="form-label white-text"> <b>CIUDAD:</b> </label>
        <input type="text" name="ciudad" id="ciudad" value="" class="form-control" placeholder="Ingrese la ciudad" required>
    </div>
    <div class="col-md-4">
        <label for="estado" class="form-label white-text"><b>ESTADO:</b></label>
        <select name="estado" id="estado" class="form-control" required>
            <option value="">Selecciona un estado de la agencia</option>
            <option value="Activo">Activo</option>
            <option value="Inactivo">Inactivo</option>
        </select>
    </div>
    <div class="col-md-4">
        <label for="telefono" class="form-label white-text"> <b>TELÉFONO:</b> </label>
        <input type="number" name="telefono" id="telefono" value="" class="form-control" placeholder="Ingrese el número" required>
    </div>
    <div class="col-md-4">
        <label for="fechaApertura" class="form-label white-text"> <b>FECHA APERTURA:</b> </label>
        <input type="date" name="fechaApertura" id="fechaApertura" value="" class="form-control" placeholder="Seleccione la fecha de apertura" required>
    </div>
    <div class="col-md-4">
        <label for="gerente" class="form-label white-text"> <b>GERENTE:</b> </label>
        <input type="text" name="gerente" id="gerente" value="" class="form-control" placeholder="Ingrese el nombre del gerente" required>
    </div>
    <div class="row">
      <div class="col-md-6">
        <label for=""> <b>Latitud:</b> </label>
        <input type="number"step="any" name="latitud" id="latitud" value="" class="form-control" placeholder="Ingrese la latitud" readonly>
      </div>
      <div class="col-md-6">
        <label for=""> <b>Longitud:</b> </label>
        <input type="number" step="any" name="longitud" id="longitud" value="" class="form-control" placeholder="Ingrese el longitud" readonly>
      </div>
    </div><br><br><br>
    <br>
    <div class="row">
      <div class="col-md-12">
        <div id="mapa" style="height:350px; width:100%; border:1px solid black;">

        </div> <br>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12 text-center">
        <button type="submit" name="button" class="btn btn-primary"><i class="fa fa-check-circle fa-bounce"></i> Gurdar</button> &nbsp;&nbsp;&nbsp;&nbsp;
        <a href="<?php echo site_url('agencias/index'); ?>" class="btn btn-danger"><i class="fa fa-xmark-circle fa-spin"></i> Cancelar</a>
      </div>

    </div>

</form>
<br>
<script type="text/javascript">
    function initMap() {
      var coordenadaCentral = new google.maps.LatLng(-0.17766723173563437, -78.46488534696985);
      var miMapa = new google.maps.Map(
        document.getElementById('mapa'),
        {
          center: coordenadaCentral,
          zoom:8 ,
          mapTypeId: google.maps.MapTypeId.ROADMAP
        }
      );
      var marcador=new google.maps.Marker({
        position:new google.maps.LatLng(-0.1855646473510723, -78.48377334240824),
        map:miMapa,
        title: 'Selecciona la ubicacion',
        draggable:true

      });
      google.maps.event.addListener(marcador,'dragend',
        function(event){
          var latitud=this.getPosition().lat();
          var longitud=this.getPosition().lng();
          document.getElementById('latitud').value = latitud;
          document.getElementById('longitud').value = longitud;
        }
    );
    }
  </script>
  <script>
    $(document).ready(function() {
        // Inicialización del plugin Bootstrap Fileinput
        $("#carnet").fileinput({
            language: 'es',
            maxFileSize: 0
        });
    });
</script>
<script type="text/javascript">
$(document).ready(function() {
  $('#formulario_agencia').validate({
        rules: {
            nombre: {
                required: true,
                letras: true,
                minlength: 3,
                primeraLetraMayuscula: true,
            },
            direccion: {
                required: true
            },
            ciudad: {
                required: true,
                letras:true,
                minlength: 3,
                primeraLetraMayuscula: true
            },
            estado: {
                required: true
            },
            telefono: {
                required: true,
                minlength: 10,
                maxlength: 10,
                digits: true
            },
            fechaApertura: {
                required: true,
                date: true,
                min: "1960-01-01",
                max: "2024-12-31"
            },
            gerente: {
                required: true,
                letras:true,
                minlength: 3,
                formatoNombre: true,
            },
            latitud: {
                required: true,
                number: true
            },
            longitud: {
                required: true,
                number: true

            }
        },
        messages: {
            nombre: {
                required: "Por favor ingrese el nombre de la agencia",
                primeraLetraMayuscula: "La primera letra del nombre debe ser mayúscula",
                minlength: "El nombre debe tener al menos {0} caracteres" // Mensaje de error personalizado para la longitud mínima
            },
            direccion: {
                required: "Por favor ingrese la dirección"
            },
            ciudad: {
                required: "Por favor ingrese la ciudad",
                minlength: "El nombre debe tener al menos {0} caracteres"
            },
            estado: {
                required: "Por favor seleccione el estado"
            },
            telefono: {
                required: "Por favor ingrese el número de teléfono",
                maxlength: "Por favor ingrese el número de 10 digitos",
                minlength: "Por favor ingrese el número de 10 digitos"
            },
            fechaApertura: {
                required: "Por favor seleccione la fecha de apertura",
                max: "Fecha fuera de rango (1960-2024)",
                min: "Fecha fuera de rango (1960-2024)"
            },
            gerente: {
                required: "Por favor ingrese el nombre del gerente",
                minlength: "El nombre debe tener al menos {0} caracteres",
                formatoNombre: "Ingrese un nombre válido (cada palabra debe comenzar con mayúscula)",
            },
            latitud: {
                required: "Por favor ingrese la latitud",
                number: "Ingrese un número válido"
            },
            longitud: {
                required: "Por favor ingrese la longitud",
                number: "Ingrese un número válido"
            }
        },
        errorElement: 'div',
        errorPlacement: function(error, element) {
            error.addClass('invalid-feedback');
            element.addClass('is-invalid');
            error.insertAfter(element);
        },
        highlight: function(element, errorClass, validClass) {
            $(element).addClass('is-invalid');
        },
        unhighlight: function(element, errorClass, validClass) {
            $(element).removeClass('is-invalid');
        }
    });
    $.validator.addMethod("letras", function(value, element) {
      return this.optional(element) || /^[a-zA-Z\sáéíóúÁÉÍÓÚüÜñÑ]+$/.test(value);
    }, "Solo se permiten letras");
    $.validator.addMethod("primeraLetraMayuscula", function(value, element) {
          return this.optional(element) || /^[A-Z].*/.test(value);
    }, "La primera letra del nombre debe ser mayúscula");
    $.validator.addMethod("formatoNombre", function(value, element) {
          return this.optional(element) || /^[A-Z][a-z]+\s[A-Z][a-z]+$/.test(value);
      }, "Cada palabra debe comenzar con mayúscula");
});

</script>
