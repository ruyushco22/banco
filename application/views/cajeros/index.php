<br>

<div class="text-center">
    <h1>
      <i class="fas fa-money-check-alt"></i>
      <b>CAJEROS</b>
    </h1>
</div>
<div class="row">
  <div class="col-md-12 text-end">

    <a href="<?php  echo site_url('cajeros/nuevo'); ?>" class="btn btn-outline-success"><i class="fa fa-plus-circle"></i> Agregar Cajero</a>
    <br> <br>
  </div>

</div>
<?php if ($listadoCajeros): ?>
    <table class="table table-bordered">
        <thead>
              <tr class="text-center">
                <th>ID</th>
                <th>MODELO</th>
                <th>NUMERO DE SERIE</th>
                <th>UBICACION CIUDAD</th>
                <th>FECHA INSTALACION</th>
                <th>AGENCIA</th>
                <th>LATITUD</th>
                <th>LONGITUD</th>
                <th>ESTADO</th>
                <th>ACCIONES</th>
              </tr>
        </thead>
        <tbody>
            <?php foreach ($listadoCajeros as $cajero): ?>
                <tr class="text-center">
                  <td><?php echo $cajero->idCajero; ?></td>
                  <td><?php echo $cajero->modelo; ?></td>
                  <td><?php echo $cajero->numeroSerie; ?></td>
                  <td><?php echo $cajero->ciudad; ?></td>
                  <td><?php echo $cajero->fechaInstalacion; ?></td>
                  <td><?php echo $cajero->nombre; ?></td>
                  <td><?php echo $cajero->latitud; ?></td>
                  <td><?php echo $cajero->longitud; ?></td>
                  <td><?php echo $cajero->estado; ?></td>
                  <td>
                    <a href="<?php echo site_url('cajeros/editar/').$cajero->idCajero; ?>"
                         class="btn btn-warning"
                         title="Editar">
                      <i class="fa fa-pen"></i>
                    </a>
                    &nbsp&nbsp
                    <a href="<?php echo site_url('cajeros/borrar/') . $cajero->idCajero; ?>" class="btn btn-danger delete-btn" title="Borrar">
                        <i class="fa-solid fa-trash"></i>
                    </a>

                  </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <script type="text/javascript">
    // Script para mostrar el cuadro de diálogo de confirmación antes de eliminar un hospital
    $('.delete-btn').on('click', function(e) {
        e.preventDefault();
        var url = $(this).attr('href');
        Swal.fire({
            title: "CONFIRMACIÓN",
            text: "¿Estás seguro de que deseas eliminar este cajero?",
            icon: "question",
            showCancelButton: true,
            confirmButtonColor: "#3085d6",
            cancelButtonColor: "#d33",
            confirmButtonText: "Sí",
            cancelButtonText: "No"
        }).then((result) => {
            if (result.isConfirmed) {
                window.location.href = url; // Redirige al URL de eliminación si se confirma
            }
        });
    });
</script>

    

<?php else: ?>
  <div class="alert alert-danger">
      No se encontro cajeros registrados
  </div>
<?php endif; ?>
