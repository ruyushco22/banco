<div class="container">
<h1>
  <br>
  <b>
    <i class="fa fa-plus-circle"></i>
    NUEVO CAJERO
  </b>
</h1>
<br>
<form class="row g-3 needs-validation custom-width-form" id="formulario_cajero" action="<?php echo site_url('cajeros/guardarCajero'); ?>" method="post" enctype="multipart/form-data" >
    <div class="col-md-4">
        <label for="modelo" class="form-label white-text"><b>MODELO:</b></label>
        <input type="text" name="modelo" id="modelo" value="" class="form-control" placeholder="Ingrese el modelo del cajero" required>
    </div>
    <div class="col-md-4">
        <label for="numeroSerie" class="form-label white-text"><b>NÚMERO DE SERIE:</b></label>
        <input type="text" name="numeroSerie" id="numeroSerie" value="" class="form-control" placeholder="Ingrese el número de serie" required>
    </div>
    <div class="col-md-4">
        <label for="ciudad" class="form-label white-text"><b>CIUDAD:</b></label>
        <input type="text" name="ciudad" id="ciudad" value="" class="form-control" placeholder="Ingrese la ciudad" required>
    </div>
    <div class="col-md-4">
        <label for="fechaInstalacion" class="form-label white-text"><b>FECHA DE INSTALACIÓN:</b></label>
        <input type="date" name="fechaInstalacion" id="fechaInstalacion" value="" class="form-control" placeholder="Seleccione la fecha de instalación" required>
    </div>
    <div class="col-md-4">
    <label for="idAgencia" class="form-label white-text"><b>AGENCIA:</b></label>
        <select name="id" id="id" class="form-control" required>
            <option value="">Selecciona una agencia</option> <!-- Opción por defecto -->
            <?php foreach ($agencias as $agencia): ?>
                <option value="<?php echo $agencia->id; ?>"><?php echo $agencia->nombre; ?></option>
            <?php endforeach; ?>
        </select>
    </div>
    <div class="col-md-4">
        <label for="estado" class="form-label white-text"><b>ESTADO:</b></label>
        <select name="estado" id="estado" class="form-control" required>
            <option value="Activo">Activo</option>
            <option value="Inactivo">Inactivo</option>
        </select>
    </div>
    <div class="row">
      <div class="col-md-6">
        <label for=""> <b>Latitud:</b> </label>
        <input type="number"step="any" name="latitud" id="latitud" value="" class="form-control" placeholder="Ingrese la latitud" readonly>
      </div>
      <div class="col-md-6">
        <label for=""> <b>Longitud:</b> </label>
        <input type="number" step="any" name="longitud" id="longitud" value="" class="form-control" placeholder="Ingrese el longitud" readonly>
      </div>
    </div><br><br><br>

    <br>
    <div class="row">
      <div class="col-md-12">
        <div id="mapa" style="height:350px; width:100%; border:1px solid black;">

        </div> <br>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12 text-center">
        <button type="submit" name="button" class="btn btn-primary"><i class="fa fa-check-circle fa-bounce"></i> Gurdar</button> &nbsp;&nbsp;&nbsp;&nbsp;
        <a href="<?php echo site_url('cajeros/index'); ?>" class="btn btn-danger"><i class="fa fa-xmark-circle fa-spin"></i> Cancelar</a>
      </div>

    </div>

</form>
<br>
<script type="text/javascript">
    function initMap() {
      var coordenadaCentral = new google.maps.LatLng(-0.17766723173563437, -78.46488534696985);
      var miMapa = new google.maps.Map(
        document.getElementById('mapa'),
        {
          center: coordenadaCentral,
          zoom:8 ,
          mapTypeId: google.maps.MapTypeId.ROADMAP
        }
      );
      var marcador=new google.maps.Marker({
        position:new google.maps.LatLng(-0.1855646473510723, -78.48377334240824),
        map:miMapa,
        title: 'Selecciona la ubicacion',
        draggable:true

      });
      google.maps.event.addListener(marcador,'dragend',
        function(event){
          var latitud=this.getPosition().lat();
          var longitud=this.getPosition().lng();
          document.getElementById('latitud').value = latitud;
          document.getElementById('longitud').value = longitud;
        }
    );
    }
  </script>
  <script>
    $(document).ready(function() {
        // Inicialización del plugin Bootstrap Fileinput
        $("#carnet").fileinput({
            language: 'es',
            maxFileSize: 0
        });
    });
</script>
<script type="text/javascript">
$(document).ready(function() {
    $('#formulario_cajero').validate({
        rules: {
            modelo: {
                required: true
            },
            numeroSerie: {
                required: true
            },
            ciudad: {
                required: true,
                letras:true,
                minlength: 3,
                primeraLetraMayuscula: true
            },
            fechaInstalacion: {
                required: true,
                date: true,
                min: "1960-01-01",
                max: "2024-12-31"
            },
            id: {
                required: true
            },
            estado: {
                required: true
            },
            latitud: {
                required: true,
                number: true
            },
            longitud: {
                required: true,
                number: true
            }
        },
        messages: {
            modelo: {
                required: "Por favor ingrese el modelo del cajero"
            },
            numeroSerie: {
                required: "Por favor ingrese el número de serie"
            },
            ciudad: {
                required: "Por favor ingrese la ciudad",
                primeraLetraMayuscula: "La primera letra del nombre debe ser mayúscula",
                minlength: "El nombre debe tener al menos {0} caracteres"
            },
            fechaInstalacion: {
                required: "Por favor seleccione la fecha de instalación",
                date: "Por favor ingrese una fecha válida",
                max: "Fecha fuera de rango (1960-2024)",
                min: "Fecha fuera de rango (1960-2024)"
            },
            id: {
                required: "Por favor seleccione una agencia"
            },
            estado: {
                required: "Por favor seleccione el estado"
            },
            latitud: {
                required: "Por favor ingrese la latitud",
                number: "Ingrese un número válido"
            },
            longitud: {
                required: "Por favor ingrese la longitud",
                number: "Ingrese un número válido"
            }
        },
        errorElement: 'div',
        errorPlacement: function(error, element) {
            error.addClass('invalid-feedback');
            element.addClass('is-invalid');
            error.insertAfter(element);
        },
        highlight: function(element, errorClass, validClass) {
            $(element).addClass('is-invalid');
        },
        unhighlight: function(element, errorClass, validClass) {
            $(element).removeClass('is-invalid');
        }
    });

    $.validator.addMethod("letras", function(value, element) {
        return this.optional(element) || /^[a-zA-Z\sáéíóúÁÉÍÓÚüÜñÑ]+$/.test(value);
    }, "Solo se permiten letras");
    $.validator.addMethod("primeraLetraMayuscula", function(value, element) {
          return this.optional(element) || /^[A-Z].*/.test(value);
    }, "La primera letra del nombre debe ser mayúscula");
    $.validator.addMethod("formatoNombre", function(value, element) {
          return this.optional(element) || /^[A-Z][a-z]+\s[A-Z][a-z]+$/.test(value);
      }, "Cada palabra debe comenzar con mayúscula");
});


</script>
