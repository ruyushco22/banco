<!DOCTYPE html>
<html>
<head>
    <title>Mapa con Marcadores</title>
    <style>
        #reporteMapa {
            height: 800px;
            width: 100%;
            border: 2px solid black;
            margin-bottom: 20px;
        }
        button {
            margin-right: 10px;
        }
    </style>
</head>
<body>
  <br>
  <button onclick="toggleMarkers('agencias')">Mostrar Agencias</button>
  <button onclick="toggleMarkers('cajeros')">Mostrar Cajeros</button>
  <button onclick="toggleMarkers('corresponsales')">Mostrar Corresponsales</button>
  <button onclick="toggleMarkers('todos')">Mostrar Todos</button>
  <br><br>
<div id="reporteMapa"></div>

<script type="text/javascript">
    var agenciaMarkers = [];
    var cajeroMarkers = [];
    var corresponsalMarkers = [];
    var map;

    function initMap() {
        var coordenadaCentral = new google.maps.LatLng(-0.152948869329262, -78.4868431364856);
        map = new google.maps.Map(document.getElementById('reporteMapa'), {
            center: coordenadaCentral,
            zoom: 10,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        });

        <?php foreach ($listadoAgencias as $agencia): ?>
        var coordenadaTemporal = new google.maps.LatLng(<?php echo $agencia->latitud; ?>, <?php echo $agencia->longitud; ?>);
        var marcador = new google.maps.Marker({
            position: coordenadaTemporal,
            map: map,
            title: '<?php echo $agencia->nombre; ?>',
            estado: '<?php echo $agencia->estado; ?>',
            icon: {
                url: 'data:image/svg+xml,%3Csvg xmlns=\'http://www.w3.org/2000/svg\' viewBox=\'0 0 384 512\'%3E%3Cpath  fill="red" d=\'M64 48c-8.8 0-16 7.2-16 16V448c0 8.8 7.2 16 16 16h80V400c0-26.5 21.5-48 48-48s48 21.5 48 48v64h80c8.8 0 16-7.2 16-16V64c0-8.8-7.2-16-16-16H64zM0 64C0 28.7 28.7 0 64 0H320c35.3 0 64 28.7 64 64V448c0 35.3-28.7 64-64 64H64c-35.3 0-64-28.7-64-64V64zm88 40c0-8.8 7.2-16 16-16h48c8.8 0 16 7.2 16 16v48c0 8.8-7.2 16-16 16H104c-8.8 0-16-7.2-16-16V104zM232 88h48c8.8 0 16 7.2 16 16v48c0 8.8-7.2 16-16 16H232c-8.8 0-16-7.2-16-16V104c0-8.8 7.2-16 16-16zM88 232c0-8.8 7.2-16 16-16h48c8.8 0 16 7.2 16 16v48c0 8.8-7.2 16-16 16H104c-8.8 0-16-7.2-16-16V232zm144-16h48c8.8 0 16 7.2 16 16v48c0 8.8-7.2 16-16 16H232c-8.8 0-16-7.2-16-16V232c0-8.8 7.2-16 16-16z\'/%3E%3C/svg%3E',
                scaledSize: new google.maps.Size(30, 30),
            }
        });
        agenciaMarkers.push(marcador);
        <?php endforeach; ?>
         hideInactiveMarkers(agenciaMarkers);
        <?php foreach ($listadoCajeros as $cajero): ?>
        var coordenadaTempora = new google.maps.LatLng(<?php echo $cajero->latitud; ?>, <?php echo $cajero->longitud; ?>);
        var marcado = new google.maps.Marker({
            position: coordenadaTempora,
            map: map,
            title: '<?php echo $cajero->ciudad; ?>',
            estado: '<?php echo $cajero->estado; ?>',
            icon: {
                url: 'data:image/svg+xml;utf8,<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512"><path fill="red" d="M64 64C28.7 64 0 92.7 0 128V384c0 35.3 28.7 64 64 64H512c35.3 0 64-28.7 64-64V128c0-35.3-28.7-64-64-64H64zM272 192H496c8.8 0 16 7.2 16 16s-7.2 16-16 16H272c-8.8 0-16-7.2-16-16s7.2-16 16-16zM256 304c0-8.8 7.2-16 16-16H496c8.8 0 16 7.2 16 16s-7.2 16-16 16H272c-8.8 0-16-7.2-16-16zM164 152v13.9c7.5 1.2 14.6 2.9 21.1 4.7c10.7 2.8 17 13.8 14.2 24.5s-13.8 17-24.5 14.2c-11-2.9-21.6-5-31.2-5.2c-7.9-.1-16 1.8-21.5 5c-4.8 2.8-6.2 5.6-6.2 9.3c0 1.8 .1 3.5 5.3 6.7c6.3 3.8 15.5 6.7 28.3 10.5l.7 .2c11.2 3.4 25.6 7.7 37.1 15c12.9 8.1 24.3 21.3 24.6 41.6c.3 20.9-10.5 36.1-24.8 45c-7.2 4.5-15.2 7.3-23.2 9V360c0 11-9 20-20 20s-20-9-20-20V345.4c-10.3-2.2-20-5.5-28.2-8.4l0 0 0 0c-2.1-.7-4.1-1.4-6.1-2.1c-10.5-3.5-16.1-14.8-12.6-25.3s14.8-16.1 25.3-12.6c2.5 .8 4.9 1.7 7.2 2.4c13.6 4.6 24 8.1 35.1 8.5c8.6 .3 16.5-1.6 21.4-4.7c4.1-2.5 6-5.5 5.9-10.5c0-2.9-.8-5-5.9-8.2c-6.3-4-15.4-6.9-28-10.7l-1.7-.5c-10.9-3.3-24.6-7.4-35.6-14c-12.7-7.7-24.6-20.5-24.7-40.7c-.1-21.1 11.8-35.7 25.8-43.9c6.9-4.1 14.5-6.8 22.2-8.5V152c0-11 9-20 20-20s20 9 20 20z"/></svg>',
                scaledSize: new google.maps.Size(32, 32)
            }
        });
        cajeroMarkers.push(marcado);
        <?php endforeach; ?>
        hideInactiveMarkers(cajeroMarkers);
        <?php foreach ($listadoCorresponsales as $corresponsal): ?>
        var coordenadaTempora = new google.maps.LatLng(<?php echo $corresponsal->latitud; ?>, <?php echo $corresponsal->longitud; ?>);
        var marcado = new google.maps.Marker({
            position: coordenadaTempora,
            map: map,
            title: '<?php echo $corresponsal->nombres; ?>',
            icon: {
                url: 'data:image/svg+xml;utf8,<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><path fill="red" d="M224 256A128 128 0 1 0 224 0a128 128 0 1 0 0 256zm-45.7 48C79.8 304 0 383.8 0 482.3C0 498.7 13.3 512 29.7 512H418.3c16.4 0 29.7-13.3 29.7-29.7C448 383.8 368.2 304 269.7 304H178.3z"/></svg>',
                scaledSize: new google.maps.Size(32, 32)
            }
        });
        corresponsalMarkers.push(marcado);
        <?php endforeach; ?>
    }

    function toggleMarkers(type) {
        switch (type) {
            case 'agencias':
                toggleMarkerVisibility(agenciaMarkers);
                break;
            case 'cajeros':
                toggleMarkerVisibility(cajeroMarkers);
                break;
            case 'corresponsales':
                toggleMarkerVisibility(corresponsalMarkers);
                break;
            case 'todos':
                toggleAllMarkers();
                break;
            default:
                break;
        }
    }

    function toggleMarkerVisibility(markers) {
        for (var i = 0; i < markers.length; i++) {
            markers[i].setMap(markers[i].getMap() ? null : map);
        }
        }
        function toggleAllMarkers() {
        for (var i = 0; i < agenciaMarkers.length; i++) {
            agenciaMarkers[i].setMap(map);
        }
        for (var i = 0; i < cajeroMarkers.length; i++) {
            cajeroMarkers[i].setMap(map);
        }
        for (var i = 0; i < corresponsalMarkers.length; i++) {
            corresponsalMarkers[i].setMap(map);
        }
    }
    function hideInactiveMarkers(markers) {
        for (var i = 0; i < markers.length; i++) {
            // Verifica si el marcador está inactivo y ocúltalo si es necesario
            if (markers[i].estado === 'Inactivo') {
                markers[i].setMap(null); // Elimina el marcador del mapa
                markers.splice(i, 1); // Elimina el marcador de la lista de marcadores
                i--;
            }
        }
    }

</script>


</body>
</html>
