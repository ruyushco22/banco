
<?php
  class Agencia extends CI_Model
  {
    function __construct()
    {
      parent::__construct();
    }
    //Insertar nuevos hospitales
    function insertar($datos){
      $respuesta=$this->db->insert("agencia",$datos);
      return $respuesta;
    }
    //Consulta de datos
    function consultarTodos(){
      $agencias=$this->db->get("agencia");
      if ($agencias->num_rows()>0) {
        return $agencias->result();
      } else {
        return false;
      }
    }
    //eliminacion de hospital por id
    function eliminar($id){
        $this->db->where("id",$id);
        $this->db->query('SET FOREIGN_KEY_CHECKS = 0');
        return $this->db->delete("agencia");

    }

    function obtenerPorId($id){
      $this->db->where("id",$id);
      $agencia=$this->db->get("agencia");
      if ($agencia->num_rows()>0) {
        return $agencia->row();
      } else {
        return false;
      }
    }
    function actualizar($id,$datos){
      $this->db->where("id",$id);
      return $this->db
                  ->update("agencia",$datos);
    }
  }//Fin de la clase
?>
