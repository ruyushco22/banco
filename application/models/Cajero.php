
<?php
  class Cajero extends CI_Model
  {
    function __construct()
    {
      parent::__construct();
    }
    //Insertar nuevos cajeros
    function insertar($datos){
      $respuesta=$this->db->insert("cajero",$datos);
      return $respuesta;
    }
    //Consulta de datos
    function consultarTodos(){
      $cajeros=$this->db->get("cajero");
      if ($cajeros->num_rows()>0) {
        return $cajeros->result();
      } else {
        return false;
      }
    }
    //eliminacion de cajero por id
    function eliminar($id){
        $this->db->where("idCajero",$id);
        return $this->db->delete("cajero");
    }

    //Consulta de un solo cajero
    function obtenerPorId($id) {
      $this->db->select('cajero.*, agencia.nombre AS nombre');
      $this->db->from('cajero');
      $this->db->join('agencia', 'cajero.id = agencia.id');

        $this->db->where('cajero.idCajero', $id);
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }
    }


    //funcion para actualizar cajeros
    function actualizar($id,$datos){
      $this->db->where("idCajero",$id);
      return $this->db
                  ->update("cajero",$datos);
    }
    function consultarTodosConAgencia()
    {
        $this->db->select('cajero.*, agencia.nombre AS nombre');
        $this->db->from('cajero');
        $this->db->join('agencia', 'cajero.id = agencia.id');
        $query = $this->db->get();
        return $query->result();
    }
  }//Fin de la clase
?>
